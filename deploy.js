const nodeCmd = require('node-cmd');
const credentials = require('./credentials/credentials.json')
const azureCredentials = require('./credentials/azure-credentials.json')

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});
var zipper = require('zip-local');
const axios = require('axios');
const fs = require('fs');

function executeCommand(command) {
    return new Promise((resolve, reject) => {
        nodeCmd.run(command, (err, data, stderr) => {
            console.log(data)
            resolve('complete')
        });
    })
}
function cloneAndCompressRepo(branchaName) {
    return new Promise(async (resolve, reject) => {
        let command = `git clone -b ${branchaName} https://${credentials.username}:${credentials.password}@bitbucket.org/myquestcoteam/candidate-test-nodejs-2021`
        await executeCommand(command);
        console.log('repo donwloaded');
        console.log('Installing packages');
        console.log('...');
        await executeCommand('cd candidate-test-nodejs-2021 && npm i ');
        console.log('Packages installed');
        console.log('Compressing Repository');
        console.log('...');
        zipper.sync.zip("./candidate-test-nodejs-2021").compress().save("candidate-test-nodejs-2021.zip");
        setTimeout(() => {
            executeCommand('rmdir /s /q candidate-test-nodejs-2021');
            resolve('complete')
        }, 5000);
    })
}
function sendZippedRepo() {
    ZipPath = 'candidate-test-nodejs-2021.zip';
    stream = fs.createReadStream(ZipPath);
    axios({
        headers: { "Content-Type": "application/x-zip-compressed" },
        method: "post",
        url: 'https://' + azureCredentials.siteName + '.scm.azurewebsites.net/api/zipdeploy',
        auth: {
            username: azureCredentials.username,
            password: azureCredentials.password
        },
        data: stream
    })
    console.log('upload complete')
}
readline.question('What is your branch name: ', branchName => {
    console.log(`Okay downloading  ${branchName}`);
    cloneAndCompressRepo(branchName).then(() => {
        console.log('compressing complete')
        sendZippedRepo()
    });
    readline.close();
})